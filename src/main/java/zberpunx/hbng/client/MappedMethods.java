package zberpunx.hbng.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;

@Service
@Slf4j
final class MappedMethods implements BeanPostProcessor {
  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) {
    for (Method method : bean.getClass().getMethods()) {
      MethodMapping mm = method.getAnnotation(MethodMapping.class);
      if (mm != null) {
        log.info("mm: bean={}, beanName={}, method={}, mm={}", bean, beanName, method, mm);
      } else {
        DownloadMethodMapping dmm = method.getAnnotation(DownloadMethodMapping.class);
        if (dmm != null) {
          log.info("dmm: bean={}, beanName={}, method={}, dmm={}", bean, beanName, method, dmm);
        }
      }
    }
    return bean;
  }
}
