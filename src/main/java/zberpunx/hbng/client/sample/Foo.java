package zberpunx.hbng.client.sample;

import org.springframework.stereotype.Controller;
import zberpunx.hbng.client.DownloadMethodMapping;
import zberpunx.hbng.client.Downloadable;
import zberpunx.hbng.client.MethodMapping;

import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

@Controller
public final class Foo {
  @MethodMapping
  public static void nop() {
    //
  }

  @DownloadMethodMapping(mediaType = IMAGE_PNG_VALUE)
  public Downloadable simpsons() {
    return () -> null;
  }
}
