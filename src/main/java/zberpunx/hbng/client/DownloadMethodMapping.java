package zberpunx.hbng.client;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;

@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface DownloadMethodMapping {
  String value() default "";

  boolean compress() default false;

  String mediaType() default APPLICATION_OCTET_STREAM_VALUE;
}
