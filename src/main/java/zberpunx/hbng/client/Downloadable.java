package zberpunx.hbng.client;

import java.io.InputStream;

public interface Downloadable {
  InputStream inputStream();
}
