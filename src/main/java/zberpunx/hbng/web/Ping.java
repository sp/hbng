package zberpunx.hbng.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
@RequestMapping(produces = TEXT_PLAIN_VALUE)
final class Ping {
  @GetMapping("/ping")
  static String ping() {
    return "pong";
  }

  @GetMapping("/пинг")
  static String пинг() {
    return "понг";
  }
}
